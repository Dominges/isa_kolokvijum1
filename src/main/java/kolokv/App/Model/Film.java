package kolokv.App.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Type;

@Entity
public class Film {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column
	@Type(type = "text")
	private String naziv;
	@Column
	private Integer uzrastOd;
	@Column
	private Integer trajanje;
	public Film() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNaziv() {
		return naziv;
	}
	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
	public Integer getUzrastOd() {
		return uzrastOd;
	}
	public void setUzrastOd(Integer uzrastOd) {
		this.uzrastOd = uzrastOd;
	}
	public Integer getTrajanje() {
		return trajanje;
	}
	public void setTrajanje(Integer trajanje) {
		this.trajanje = trajanje;
	}
	public Film(Long id, String naziv, Integer uzrastOd, Integer trajanje) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.uzrastOd = uzrastOd;
		this.trajanje = trajanje;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Film other = (Film) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	

}
