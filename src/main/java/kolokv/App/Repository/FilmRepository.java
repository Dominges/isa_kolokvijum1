package kolokv.App.Repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import kolokv.App.Model.Film;

@Repository
public interface FilmRepository extends PagingAndSortingRepository<Film, Long> {

}
