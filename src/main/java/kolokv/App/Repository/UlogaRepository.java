package kolokv.App.Repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import kolokv.App.Model.Uloga;

@Repository
public interface UlogaRepository extends PagingAndSortingRepository<Uloga, Long>{

}
