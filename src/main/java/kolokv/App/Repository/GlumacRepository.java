package kolokv.App.Repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import kolokv.App.Model.Glumac;

@Repository
public interface GlumacRepository extends PagingAndSortingRepository<Glumac, Long>{

}
