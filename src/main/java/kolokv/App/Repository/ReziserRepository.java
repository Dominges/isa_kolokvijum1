package kolokv.App.Repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import kolokv.App.Model.Reziser;

@Repository
public interface ReziserRepository extends PagingAndSortingRepository<Reziser, Long>{

}
