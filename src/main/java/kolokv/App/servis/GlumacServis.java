package kolokv.App.servis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kolokv.App.Model.Glumac;
import kolokv.App.Repository.GlumacRepository;

@Service
public class GlumacServis {
	
	@Autowired
	GlumacRepository glumacRepository;
	
	public Glumac findOne(Long id) {
		return glumacRepository.findById(id).orElse(null);
	}
	
	public Iterable<Glumac> findAll(){
		return glumacRepository.findAll();
	}
	
	public void remove(Long id) {
		glumacRepository.deleteById(id);
	}
	
	public Glumac save(Glumac glumac) {
		return glumacRepository.save(glumac);
	}

}
