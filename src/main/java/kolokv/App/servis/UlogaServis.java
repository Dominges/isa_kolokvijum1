package kolokv.App.servis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kolokv.App.Model.Uloga;
import kolokv.App.Repository.UlogaRepository;

@Service
public class UlogaServis {
	
	@Autowired
	UlogaRepository ulogaRepository;
	
	public Uloga findOne(Long id) {
		return ulogaRepository.findById(id).orElse(null);
	}
	
	public Iterable<Uloga> findAll(){
		return ulogaRepository.findAll();
	}
	
	public void remove(Long id) {
		ulogaRepository.deleteById(id);
	}
	
	public Uloga save(Uloga uloga) {
		return ulogaRepository.save(uloga);
	}

}
