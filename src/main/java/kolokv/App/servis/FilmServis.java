package kolokv.App.servis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kolokv.App.Model.Film;
import kolokv.App.Repository.FilmRepository;

@Service
public class FilmServis {
	
	@Autowired
	FilmRepository filmRepository;
	
	public Film findOne(Long id) {
		return filmRepository.findById(id).orElse(null);
	}
	
	public Iterable<Film> findAll(){
		return filmRepository.findAll();
	}
	
	public void remove(Long id) {
		filmRepository.deleteById(id);
	}
	
	public Film save(Film film) {
		return filmRepository.save(film);
	}

}
