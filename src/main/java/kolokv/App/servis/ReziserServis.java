package kolokv.App.servis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kolokv.App.Model.Reziser;
import kolokv.App.Repository.ReziserRepository;

@Service
public class ReziserServis {
	
	@Autowired
	ReziserRepository reziserRepository;
	
	public Reziser findOne(Long id) {
		return reziserRepository.findById(id).orElse(null);
	}
	
	public Iterable<Reziser> findAll(){
		return reziserRepository.findAll();
	}
	
	public void remove(Long id) {
		reziserRepository.deleteById(id);
	}
	
	public Reziser save(Reziser resizer) {
		return reziserRepository.save(resizer);
	}

}
